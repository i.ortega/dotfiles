#!/bin/bash

if [ $# -lt 1 ]
then
	echo "Usage: $0 (toggle|get)"
	exit 1
fi

EXT_OUT="DP2"
INT_OUT="LVDS1"
# External location may be "--left-of" or "--right-of" or "--above" or "--below"
EXT_LOCATION="--right-of"
MODE="--mode 2560x1440"
 
EXT_STATE=$(xrandr | sed -n "/$EXT_OUT/,/$INT_OUT/p" | grep '*')
IS_EXT_CONNECTED=$(xrandr | grep -q "$EXT_OUT connected")

case "$1" in
	toggle)
		if [ -z "$EXT_STATE" ]
		then
			[ "$IS_EXT_CONNECTED" -ne 0 ] && exit
			xrandr --output $EXT_OUT $EXT_LOCATION $INT_OUT $MODE
		else
			xrandr --output $EXT_OUT --off 
		fi
		xset dpms force on
		;;
	get)
		[ -z "$EXT_STATE" ] && echo "int" || echo "ext"
		;;
	 *)
		echo "Unknown option '$1'"
		exit 1;
		 ;;
esac



