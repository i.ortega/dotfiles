#!/usr/bin/env sh

# This script sets the statusbar with the xsetroot command at the end. Have it
# started by ~/.xinitrc or ~/.xprofile.

# Set the deliminter character.
delim="|"

# testweather checks to see if the most recent forecast is up to date.  If it
# isn't, it downloads a new weather forecast, then signals to update the
# statusbar. Gets weather report from wttr.in.
testweather() { \
    [ "$(stat -c %y "$HOME/.local/share/weatherreport" 2>/dev/null | \
    cut -d' ' -f1)" != "$(date '+%Y-%m-%d')" ] &&
    ping -q -c 1 1.1.1.1 >/dev/null &&
    curl -s "wttr.in/$location" > "$HOME/.local/share/weatherreport" &&
    notify-send "🌞 Weather" "New weather forecast for today." &&
    refbar
}

getbattery() {
    batpath="/sys/class/power_supply/BAT0/"
    for kv in "$(cat $batpath/uevent)"; do
        kv="$(printf "%b\n" "$kv" | sed "s| ||g")"
        export $kv
    done
    enow="$POWER_SUPPLY_ENERGY_NOW"
    efull="$POWER_SUPPLY_ENERGY_FULL"
    vnow="$POWER_SUPPLY_VOLTAGE_NOW"
    pwrsupstat="$POWER_SUPPLY_STATUS"

    r="$(printf "%b\n" "($enow * 100 / $vnow) * 100 / ($efull * 100 / $vnow)" |bc)"
    r="${r}%"
    [ "$pwrsupstat" = "Charging" ] && r="${r}+" || r="${r}-"
    printf "%b\n" "$r"
}

getdisk() {
    percent="$(df -h | sed '2q;d' | awk '{print $5}')"
    printf "%b\n" "/: $percent"
}

getmem() {
    printf "%b\n" "$(free -m | sed '2q;d' | awk '{print $3" / "$2}')"
}

getiwlink() {
	s=$(/usr/sbin/iw dev wlp2s0 link | head -1 | awk '{print $1}')
    r=""
	if [ "$s" = "Connected" ]; then
        r=" $(/usr/sbin/iw dev wlp2s0 link |grep signal | awk '{print $2" "$3}')"
	else
        s=$(/usr/sbin/iw dev enp0s25 link | head -1 | awk '{print $1}')
        if [ "$s" = "Connected" ]; then
            r="eth"
        else
            r="off"
        fi
	fi

	printf "%b\n" $r
}

getlayout() {
    setxkbmap -query | grep layout | awk '{print $2}'
}

getfreqs() {
	# freqs=$(cat /sys/devices/system/cpu/*/cpufreq/scaling_cur_freq)
	# ret=""
	# for f in $freqs
	# do
	# 	f="$(printf "%b\n" "$f / 100000" |bc)"
	# 	f=$(printf "0.%0.2d" $f)
	# 	[ -z "$ret" ] && ret=$f || ret="${ret} $f"
	# done


    freq="$(cat /tmp/cpu-freq)"
    printf "%b\n" "$freq%"
}

gettemp() {
	# XXX handle more sensors
	t=$(cat /sys/class/hwmon/hwmon*/temp1_input |head -1)
	t=$(printf "%b\n" $t / 1000 |bc)
	r=$(printf "%d" $t)
	printf "%b\n" "${r}C"
}

# Here is the (big) function that outputs the appearance of the statusbar. It
# can really be broken down into many submodules which I've commented and
# explained.
status() { \

    # Get current mpd track filename or artist - title if possible.
# mpc -f "[[%artist% - ]%title%]|[%file%]" 2>/dev/null | grep -v "volume:" | \
# head -n 1 && printf "%b\n" "$delim"

    # If the weather report is current, show daily precipitation chance,
    # low and high.  Don't even bother to understand this one unless you
    # have all day. Takes the weather report format from wttr.in and makes
    # it look like how I want it for the status bar.
    # [ "$(stat -c %y "$HOME/.local/share/weatherreport" 2>/dev/null | \
    #     cut -d' ' -f1)" = "$(date '+%Y-%m-%d')" ] &&
    #     sed '16q;d' "$HOME/.local/share/weatherreport" | grep -wo "[0-9]*%" | \
    #     sort -n | sed -e '$!d' | sed -e "s/^/ /g" | tr -d '\n' &&
    #     sed '13q;d' "$HOME/.local/share/weatherreport" | \
    #     grep -o "m\\(-\\)*[0-9]\\+" | sort -n -t 'm' -k 2n | \
    #     sed -e 1b -e '$!d' | tr '\n|m' ' ' | \
    #     awk '{print " ",$1 "°","",$2 "°"}' && printf "%b\n" "$delim"

    printf "%b\n" " "

    printf "%b\n" "$(getlayout)"
    printf "%b\n" "$delim"

    # Get the volume of ALSA's master volume output.  Show an icon if or
    # not muted.
    amixer get Master | grep -o "[0-9]*%\|\[on\]\|\[off\]" | \
        sed "s/\[on\]//;s/\[off\]//" | head -n 2

    printf "%b\n" "$delim"

    printf "%b\n" "$(getiwlink)"
    # Wifi quality percentage and  icon if ethernet is connected.
    # grep "^\s*w" /proc/net/wireless |awk '{ print "", int($3 * 100 / 70) "%" }'
    # sed "s/down//;s/up//" /sys/class/net/e*/operstate
    # pingres="$(ping -c 3 8.8.8.8 | grep '100% packet loss' )"
    # if [ "$pingres" -ne "" ]; then
    #     printf "%b\n" "µ"
    # else
    #     printf "%b\n" ""
    # fi
    printf "%b\n" "$delim"

    printf "%b\n" "$(getfreqs)"
    printf "%b\n" "$delim"

    printf "%b\n" "$(getdisk)"
    printf "%b\n" "$delim"

    printf "%b\n" "$(getmem)"
    printf "%b\n" "$delim"

    # Show unread mail if mutt-wizard is installed.
    # command -v mw >/dev/null 2>&1 &&
    #     printf "%b\n" "$delim" &&
    #     du -a ~/.local/share/mail/*/INBOX/new/* 2>/dev/null | wc -l | \
    #     sed 's/^/:/'
    #         printf "%b\n" "$delim"

    # Will show all batteries with approximate icon for remaining power.
    if [ -e /sys/class/power_supply/BAT? ]; then
        # for x in /sys/class/power_supply/BAT?/capacity;
        # do
        #     case "$(cat $x)" in
        #         100|9[0-9])	printf "%b\n" "" ;;
        #         8[0-9]|7[0-9])	printf "%b\n" "" ;;
        #         6[0-9]|5[0-9])	printf "%b\n" "" ;;
        #         4[0-9]|3[0-9])	printf "%b\n" "" ;;
        #         *)		printf "%b\n" "" ;;
        #     esac
        # done
        printf "%b\n" "$(getbattery)"
        printf "%b\n" "$delim"
    fi

    # Date and time.
    # date '+%Y %b %d (%a) %I:%M%p'
    date '+%Y %b %d (%a) %H:%M'
}

update() { \
    # So all that big status function was just a command that quicking gets
    # what we want to be the statusbar. This xsetroot command is what sets
    # it. Note that the tr command replaces newlines with spaces. This is
    # to prevent some weird issues that cause significant slowing of
    # everything in dwm. Note entirely sure of the cause, but again, the tr
    # command easily avoids it.
    xsetroot -name "$(status | tr '\n' ' ')" &
    # wait

    # Check to see if new weather report is needed.
    # testweather &
    # wait
    [ -n "$sleep_pid" ] && kill "$sleep_pid"
}

# Handle SIGTRAP signals sent by refbar to update the status bar immediately.
trap update TRAP

update
while :; do
    # Sleep for a minute after changing the status bar before updating it
    # again. We run sleep in the background and use wait until it finishes,
    # because traps can interrupt wait immediately, but they can't do that
    # with sleep.
    sleep infinity &
    sleep_pid="$!"
    wait
done
