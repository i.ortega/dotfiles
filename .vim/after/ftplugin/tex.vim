" LaTeX                                                                    
map <buffer> <C-m> :!zathura %:p:r.pdf & disown <ENTER><ENTER>
map <buffer> <C-l> :w <bar> :!origindir=$(pwd) && cd %:p:h && pdflatex -synctex=1 -interaction=nonstopmode --shell-escape -output-directory %:p:h %:p && cd $origindir<ENTER>

" imap <C-CR> <CR>\\item 
