#!/usr/bin/env sh

url="$(xclip -o -selection clipboard)"

filename="$(dash $TSCRIPTS/download-video.sh "$url")"

printf "%b\n" "$filename"
