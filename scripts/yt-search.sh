#!/usr/bin/env sh

search="$(rofi -dmenu)"

if [ -n "$search" ]; then

    id="$(youtube-dl --get-id "ytsearch:$search")"

    link="https://invidio.us/watch?v=$id"

    $VIDEOPLAYER "$link" 1>&2

    printf "%b\n" "$link"

fi
