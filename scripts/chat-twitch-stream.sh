#!/usr/bin/env bash

streamer="$(rofi -dmenu)"

if [ -n "$streamer" ]; then
    if [ "$(youtube-dl -e "https://www.twitch.tv/$streamer" 2>/dev/null)" ]; then
        notify-send "$streamer found"
        $BROWSER "https://www.twitch.tv/popout/$streamer/chat?popout="
    else
        notify-send "$streamer NOT FOUND or NOT STREAMING"
    fi
fi
