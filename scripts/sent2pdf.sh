#!/bin/sh

slides=$(( $(perl -0777 -pe 's/\n\n\n/\n\n/igs' < "$1" | grep -cvP '\S') + 1 ))
[ -z "$2" ] && out="$HOME/out.pdf" || out="$2"

sent "$1" &

while ! xdotool search --onlyvisible --class sent
do
    sleep 1
done
window=$(xdotool search --onlyvisible --class sent)

folder="/tmp/sent2pdf"
if [ ! -d "$folder" ]; then
    mkdir "$folder"
else
    [ -n "$folder" ] && rm -r "$folder/*"
fi

i="0"

killall unclutter 2> /dev/null


wait4stop="/tmp/stop-sent2pdf"
touch $wait4stop
while [ -f $wait4stop ] && [ "$slides" -gt "$i" ]
do
    i="$(( $i + 1 ))"
    import "$folder/$i.png" -window current
    xdotool key --window "$window" space
    sleep 0.3

done

rm "$wait4stop" 2>/dev/null


files="$(ls -v $folder/*.png | tr '\n' ' ')"

convert $files "$out"

unclutter --jitter 100 -b

rm -r "$folder"
