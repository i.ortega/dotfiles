#!/usr/bin/env sh

site="$(find "$PASSWORD_STORE_DIR" -name "*.gpg" -type f | \
    sed "s|^$PASSWORD_STORE_DIR/||" | sed "s|\\.gpg$||" | rofi -dmenu -p "Entry")"

[ -z "$site" ] && exit 0

pass remove "$site"
