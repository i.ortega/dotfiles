#!/usr/bin/env sh

# Not working if there is no navigation bar
xdotool key ctrl+l
xdotool key ctrl+c
# xdotool type yy

# link="$(xclip -o -selection clipboard | sed -E 's|([^&]*)(&)(.*)|\1|')"
link="$(xclip -o | sed -E 's|([^&]*)(&)(.*)|\1|')"

$VIDEOPLAYER "$link"
