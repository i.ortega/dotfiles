#!/usr/bin/env bash

mod=$(echo "$1 % 8" | bc)
blocks=$(echo "$1 / 8" | bc)

for _ in $(seq 1 "$blocks"); do
    printf "█"
done

case "$mod" in
    1)
        printf "▏"
        ;;
    2)
        printf "▎"
        ;;
    3)
        printf "▍"
        ;;
    4)
        printf "▌"
        ;;
    5)
        [ "$1" -lt 96 ] && printf "▋"
        ;;
    6)
        [ "$1" -lt 96 ] && printf "▊"
        ;;
    7)
        [ "$1" -lt 96 ] && printf "▉"
        ;;
esac
