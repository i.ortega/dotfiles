#!/usr/bin/env sh

inode="$(find "$HOME" -type f | sed "s|^$HOME/||")"
inode=""${inode}"$(find "$DEVICES_FOLDER" -type f)"

file="$(printf "%b\n" "${inode}" | dmenu -i -l 10)"

[ -z "$file" ] && exit 0

directories="$(find "$HOME" -type d | sed "s|^$HOME/||")"
directories=""${directories}"$(find "$DEVICES_FOLDER" -type d)"

newdir="$(printf "%b\n" "${directories}" | dmenu -i -l 10)"

[ -z "$newdir" ] && exit 0

size="$(stat --printf="%s" "$HOME/$file")"

i=0
while [ "$(printf "%b\n" "$size >= 1024" | bc)" -eq "1" ]
do
    size="$(printf "%b\n" "$size/1024" | bc -l )"
    i=$(( $i + 1 ))
done

case "$i" in
    0) unit="B" ;;
    1) unit="KB" ;;
    2) unit="MB" ;;
    3) unit="GB" ;;
    4) unit="TB" ;;
esac

notify-send -u "normal" "Size ($unit) = $size"
rsync -ah --progress "$HOME/$file" "$newdir"
