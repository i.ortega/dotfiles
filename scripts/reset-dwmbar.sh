#!/usr/bin/env sh

resetlockfile="/tmp/dwmbar-reset-lock"
[ ! -f "$resetlockfile" ] && {
    touch "$resetlockfile"
    p_num=$(pgrep -a busybox | grep "busybox .*/dwmbar.sh" | cut -d ' ' -f 1)
    [ -n "$p_num" ] && kill -s 5 "$p_num"
    rm "$resetlockfile"
}
