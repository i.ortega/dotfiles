#!/usr/bin/env sh

day="$(date "+%d %m")"

case "$day" in
    "31 10")
        folder="$HALLOWEEN_WALLPAPERS"
        ;;
    *)
        folder="$WALLPAPERS/non-explicit"
        ;;
esac

list=$(find "$folder" -type f -not -regex ".*\.md")

num_lines=$(printf "%b\n" "$list" | wc -l)

num=$(shuf -i "1-$num_lines" -n 1)

file=$(printf "%b\n" "$list" | sed "$num q;d")

feh --bg-fill "$file"
