#!/usr/bin/env sh

page="$(find "$PASSWORD_STORE_DIR/" -not -path "$PASSWORD_STORE_DIR/.git/*" \
    -type f | sed "s|.gpg$||")"
page_relative="$(printf "%b\n" "$page" | sed "s|$PASSWORD_STORE_DIR||" | \
    grep -E "^(/[^/]+)(/[^/]+)(/[^/]+)?/?$")"

page_relative="$(printf "%b\n" "$page_relative" | sort | dmenu -l 15 -i)"
[ -z "$page_relative" ] && exit 0
[ -z "$(printf "%b\n" "$page_relative" | grep -E "^(/[^/]+)(/[^/]+)(/[^/]+)?/?$")" ] && \
    notify-send -t 11000 "The reason to show first level folders is to make use\
 of <TAB> on dmenu and create new password inside them. There should NOT be\
 passwords with first level names." && exit 1

page="$PASSWORD_STORE_DIR$page_relative"

info="$(printf "%b\n" "Yes\nNo" | rofi -dmenu -i -p "Change info?")"

[ -z $(printf "%b\n" "$info" | grep -E "(Yes|No)") ] && exit 0

change="$(printf "%b\n" "Yes\nNo" | rofi -dmenu -i -p "Change password?")"
if [ "$change" = "Yes" ]; then
    generate="$(printf "%b\n" "Yes\nNo" | rofi -dmenu -i -p "Generate password?")"
    [ -z $(printf "%b\n" "$generate" | grep -E "(Yes|No)") ] && exit 0
fi

passw="$(pass show "$page_relative")"
contents_nopass="$(printf "%b\n" "$passw" | tail -n +2)"
passw="$(printf "%b\n" "$passw" | head -n 1)"

if [ "$info" = "Yes" ]; then
    file="/tmp/change-pass$(date +%s)"
    printf "%b\n" "$contents_nopass" > "$file"
    cp "$file" "$file-copy"
    $TSCRIPTS/run-on-terminal.sh $EDITOR $file
    # [ -z "${TERMINAL##*termite*}" ] && $TERMINAL -e "$EDITOR $file" || \
    #     $TERMINAL -e $EDITOR $file
    [ -z "$(cat $file)" ] && notify-send "Operation CANCELLED" && \
        rm "$file" "$file-copy" && exit 1
    [ -z "$(diff "$file" "$file-copy")" ] && notify-send "Operation CANCELLED" \
        && rm "$file" "$file-copy" && exit 1
    contents_nopass="$(cat "$file")"
    rm "$file" "$file-copy"
fi


if [ "$change" = "Yes" ]; then
    if [ "$generate" = "Yes" ]; then
        contents="$(pwgen -ysBv 15 -N 1)"
    else
        tries=0
        while [ "$tries" -ne 3 ]; do
            passwd="$(rofi -i -dmenu -password -p "Type")"
            passwdcheck="$(rofi -i -dmenu -password -p "Type Again")"
            [ "$passwd" != "$passwdcheck" ] || break \
                && notify-send "Passwords are not equal"
            tries="$(( $tries + 1 ))"
        done
        [ "$passwd" != "$passwdcheck" ] && notify-send "Operation CANCELLED" && \
            exit 1
        contents="$passwd"
    fi

    contents="$contents\n$contents_nopass"
else
    contents="$passw\n$contents_nopass"
fi

if [ -n "$contents" ]; then
    printf "%b\n" "$contents\n" | pass insert -m "$page_relative" || \
            (notify-send "Error: pass insert" && exit 1)
fi
