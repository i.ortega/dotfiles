#!/usr/bin/env sh

res="$(printf "%b\n" "Yes\nNo" | dmenu -nb "#DF7401" -sb "#8A0886" -nf "#ffffff" \
    -sf "#ffffff"  -p "Are you sure u want to reboot?")"

[ "$res" = "Yes" ] && loginctl reboot
