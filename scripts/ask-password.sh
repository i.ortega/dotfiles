#!/usr/bin/env sh

tries=0
while [ "$tries" -ne 3 ]; do
    passwd="$($SUDO_ASKPASS)"
    [ -z "$passwd" ] && exit 1
    passwdcheck="$($SUDO_ASKPASS)"
    [ -z "$passwdcheck" ] && exit 1

    [ "$passwd" != "$passwdcheck" ] || break \
        && notify-send "Passwords are not equal"
    tries="$(( $tries + 1 ))"
done
[ "$passwd" != "$passwdcheck" ] && notify-send "Operation CANCELLED" && \
    exit 1

printf "%b\n" "$passwd"
