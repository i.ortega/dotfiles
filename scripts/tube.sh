#!/usr/bin/env sh

[ "$1" = "" ] || url="$1"

[ "$url" = "" ] && notify-send "Tube.sh" "Nothing will be downloaded" && exit 0

$VIDEOPLAYER "$url"
