#!/usr/bin/env sh

site="$(find "$PASSWORD_STORE_DIR" -name "*.gpg" -type f | \
    sed "s|^$PASSWORD_STORE_DIR/||" | sed "s|\\.gpg$||" | \
    rofi -dmenu -i -p "Entry")"

[ -z "$site" ] && exit 0

comments="$(pass show "$site" | sed '1d')"

# num="$(printf "%b\n" "$comments" | grep -n "comments:" | sed '1q;d' | \
#     awk '{print $1}' | sed -r 's|([^:]*):.*|\1|')"

# [ "$num" -ne 1 ] && comments="$(printf "%b\n" "$comments" | sed "1,$(( $num - 1 ))d")"

notify-send "This terminal will close in 45 seconds"
    
dash $TSCRIPTS/run-on-terminal.sh "printf "%b\n" \"$comments\" && sleep 45" 
# [ -z "${TERMINAL##*termite*}" ] && \
#     $TERMINAL -e "$TSCRIPTS/eval.sh \"printf "%b\n" \\\"$comments\\\" && sleep 45\"" \
#     || $TERMINAL -e $TSCRIPTS/eval.sh "printf "%b\n" \"$comments\" && sleep 45"
