#!/usr/bin/env sh

if [ -z "$2" ]; then
    [ "$1" != "--print" ] && site="$1" || prnt="1"
else
    if [ "$2" = "--print" ]; then
        prnt="1"
        site="$1"
    elif [ "$1" = "--print" ]; then
        site="$2"
        prnt="1"
    fi
fi



[ -z "$site" ] && site="$(find "$PASSWORD_STORE_DIR" -name "*.gpg" -type f | \
    sed "s|^$PASSWORD_STORE_DIR/||" | sed "s|\\.gpg$||" | \
    rofi -dmenu -i -p "Entry")"

[ -z "$site" ] && exit 0


if [ "$prnt" -eq 1 ]; then
    pass show "$site" | grep "login: " | sed -r "s|^login: (.*)($)|\1|" 
else
    pass show "$site" | grep "login: " | \
    sed -r "s|^login: (.*)($)|\1|" | xclip -i -selection clipboard
fi
