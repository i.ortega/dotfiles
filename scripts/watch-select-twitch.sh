#!/usr/bin/env sh

watch="$(cat $FAV_STREAMERS | rofi -dmenu -matching fuzzy)"

[ -n "$watch" ] && {
    if [ -n "$(youtube-dl --get-id "https://www.twitch.tv/$watch")" ]; then
        dash $TSCRIPTS/watch-video-select-format.sh \
            "https://www.twitch.tv/$watch"
    else
        notify-send "Not Found Streamer:" "$watch"
    fi
}
