#!/usr/bin/env bash

watch="$(cat $FAV_STREAMERS | rofi -dmenu -matching fuzzy)"

[ -n "$watch" ] && $BROWSER "https://www.twitch.tv/popout/$watch/chat?popout="
