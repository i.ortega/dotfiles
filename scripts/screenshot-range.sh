#!/usr/bin/env sh

# origin="$(pwd)"
# cd "$SCREENSHOTS"

killall unclutter 2> /dev/null

eguna="$(date -I)"

argazki_izena="Screenshot-$eguna"

gaurko_kop="$(ls -l "$SCREENSHOTS" | grep "$argazki_izena" | wc -l)"

if [ "$gaurko_kop" -eq "0" ]; then
    import "$SCREENSHOTS/$argazki_izena.png"
else
    import "$SCREENSHOTS/$argazki_izena-$gaurko_kop.png"
fi

unclutter --jitter 100 -b

# cd "$origin"
