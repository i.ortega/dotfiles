#!/usr/bin/env sh

# if ! xdotool search --name "$MUSICPLAYERUI"; then
if [ "$(command -v disown > /dev/null)" ]; then
    st -c "Music Player" -e $MUSICPLAYERUI & disown
else
    st -c "Music Player" -e $MUSICPLAYERUI &
fi
# fi
