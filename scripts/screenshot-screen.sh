#!/usr/bin/env sh

eguna="$(date -I)"

argazki_izena="Screenshot-$eguna"

gaurko_kop="$(ls -l "$SCREENSHOTS" | grep "$argazki_izena" | wc -l)"

if [ "$gaurko_kop" -eq "0" ]; then
    import -window root "$SCREENSHOTS/$argazki_izena.png"
else
    import -window root "$SCREENSHOTS/$argazki_izena-$gaurko_kop.png"
fi
