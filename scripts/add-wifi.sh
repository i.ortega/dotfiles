#!/usr/bin/env sh

scan="$(sudo -A iw dev wlp2s0 scan)" || exit 1
ssids="$(printf "%b\n" "$scan" | grep "SSID:" | awk '{$1 = ""; print $0}' | sed 's| ||')"

[ -z "$1" ] && myssid="$(printf "%b\n" "$ssids" | dmenu -i -p "SSID")" \
    || myssid="$1"

[ -z "$myssid" ] && exit 1

already_added="$(sudo -A cat /etc/wpa_supplicant/wpa_supplicant.conf | \
    grep -E "^[^\#]*ssid=\"$myssid\"")"
[ -n "$already_added" ] && exit 1

if [ -z "$2" ]; then
    passphrase="$(dash $SCRIPTS/ask-password.sh)" || exit 1
    [ -z "$passphrase" ] && exit 1
else
    passphrase="$2"
fi

[ -z "$passphrase" ] && exit 1

file="/tmp/add-wifi$(date +%s)"
wpa_passphrase "$myssid" "$passphrase" > $file

cat "$file" | sudo -A tee -a /etc/wpa_supplicant/wpa_supplicant.conf

sudo -A wpa_supplicant -B -i wlp2s0 -c $file

rm "$file"
