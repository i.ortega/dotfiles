#!/usr/bin/env sh


level=$(amixer get Master | grep -o "[0-9]*%\|\[on\]\|\[off\]" |
    sed "s/\[on\]//;s/\[off\]//" | head -n 2)

bar=$(echo "$level" | head -n 1 | tr '%' ' ' |
          xargs dash "$SCRIPTS/progress-bar.sh")

echo "$level" | xargs echo |
    xargs -I{} notify-send -u low -t 3000 "Volume: {}" "$bar"
