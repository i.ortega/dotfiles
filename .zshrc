# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

if [[ "$TERM" == "dumb" ]]
then
  unsetopt zle
  unsetopt prompt_cr
  unsetopt prompt_subst
  if whence -w precmd >/dev/null; then
	  unfunction precmd
  fi
  if whence -w preexec >/dev/null; then
	  unfunction preexec
  fi
  PS1='$ '
fi
# if [ $TERM = tramp ]; then
# 	unset RPROMPT
# 	unset RPS1
# 	PS1="$ "
# 	unsetopt zle
# 	unsetopt rcs  # Inhibit loading of further config files
# fi

# Path to your oh-my-zsh installation.
export ZSH="/home/initega/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="xiong-chiamiov"
plugins=(
	# git
	chucknorris
	# man
	# pip
	# cp
	# z
	zsh-autosuggestions
	zsh-syntax-highlighting
	zsh-history-substring-search
	)

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

source $ZSH/oh-my-zsh.sh
source $HOME/.profile
# export PATH="$HOME/bin:$HOME/.local/bin:$PATH"
# source $HOME/.alias
# source $HOME/ros/serial/setup.zsh
# source /usr/share/rosbash/roszsh

DISABLE_UPDATE_PROMPT=true

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=white'
ZSH_AUTOSUGGEST_STRATEGY=(history completion)

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
ZSH_HIGHLIGHT_STYLES[alias]='fg=magenta'
case "$TERM" in
	"eterm-color")
		ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=green'
		ZSH_HIGHLIGHT_STYLES[command]='fg=blue'
		ZSH_HIGHLIGHT_STYLES[builtin]='fg=blue'
		;;
	*)
		ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=#2fc81e'
		ZSH_HIGHLIGHT_STYLES[command]='fg=#9ce8f8'
		ZSH_HIGHLIGHT_STYLES[builtin]='fg=#9ce8f8'
		;;
esac
ZSH_HIGHLIGHT_STYLES[path]='fg=cyan'

HISTORY_SUBSTRING_SEARCH_FUZZY="t"
DISABLE_MAGIC_FUNCTIONS=true


### Fix slowness of pastes with zsh-syntax-highlighting.zsh
pasteinit() {
  OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
  zle -N self-insert url-quote-magic # I wonder if you'd need `.url-quote-magic`?
}

pastefinish() {
  zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish
### Fix slowness of pastes


bindkey -v

bindkey '^[OA' history-substring-search-up
bindkey '^[OB' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
# Better searching in command mode
bindkey -M vicmd '?' history-incremental-search-backward
bindkey -M vicmd '/' history-incremental-search-forward
# Easier, more vim-like editor opening
# bindkey -M vicmd v edit-command-line

# precmd() { RPROMPT="" }
# function zle-line-init zle-keymap-select {
#    VIM_PROMPT="%{$fg_bold[yellow]%} [% NORMAL]%  %{$reset_color%}"
#    RPS1="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/} $EPS1"
#    zle reset-prompt
# }

# zle -N zle-line-init
# zle -N zle-keymap-select
# Updates editor information when the keymap changes.
function zle-keymap-select() {
	zle reset-prompt
	zle -R
}

zle -N zle-keymap-select

function vi_mode_prompt_info() {
	echo "%{$fg[yellow]%} ${${KEYMAP/vicmd/[% NORMAL]%}/(main|viins)/[% INSERT]%}"
}

# define right prompt, regardless of whether the theme defined it
RPS1='$(vi_mode_prompt_info)'
RPS2=$RPS1

export KEYTIMEOUT=1

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# neofetch
exec fish
