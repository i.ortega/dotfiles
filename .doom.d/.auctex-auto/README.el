(TeX-add-style-hook
 "README"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:org5353487"
    "sec:orgd6bd057"
    "sec:orgf9e85bd"
    "sec:org77e416c"
    "sec:orgdeecf62"
    "sec:org7503de0"
    "sec:org50def99"
    "sec:orgd580bdf"
    "sec:org9fa885d"
    "sec:org0b3b8d4"
    "sec:org6f2c868"
    "sec:org3a32ad2"
    "sec:org59d77af"
    "sec:org8e3b2e1"
    "sec:org55dfd9d"
    "sec:org18f354e"
    "sec:org7f4a24e"
    "sec:orgb0c6ce7"
    "sec:org7483384"
    "sec:orgfe99b0a"))
 :latex)

