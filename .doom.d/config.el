;; -*- lexical-binding: t -*-
(setq doom-font (font-spec :family "monospace" :width 'normal :size 17)
      doom-variable-pitch-font (font-spec :family "monospace"))
(setq doom-theme 'doom-gruvbox)
(setq display-line-numbers-type 'relative)
(setq org-directory "~/org/")
(setq sh-symbols
      '(sed tail printf echo pidof top dmenu rofi killall sed awk tee basename
            ln mkdir rm ssh sleep source ps bash python perl Rscript wget
            bunzip2 bzip2 zip unzip gzip gunzip find ls cat egrep grep mv cp
            chmod tar stty export spark-shell spark-submit hadoop pyspark aws
            dash xdotool xprop 7z p7z zsh fish expr command disown pv alias
            ))

(setq prelude-symbols
      '(not otherwise maybe either fst snd curry uncurry compare min max succ
            pred toEnum fromEnum enumFrom enumFromThen enumFromTo enumFromThenTo
            minBound maxBound abs negate signum fromInteger show showsPrec showList
            toInteger toRational quot rem div mod quotRem divMod toInteger recip
            fromRational pi exp log sqrt logBase sin cos tan asin acos atan sinh cosh
            tanh asinh acosh atanh map properFraction truncate round ceiling floor
            floatRadix floatDigits floatRange decodeFloat encodeFloat exponent
            significand scaleFloat isNaN isInfinite isDenormalized isNegativeZero
            isIEEE atan2 subtract odd even gcd lcm fromIntegral realToFrac mempty
            mappend mconcat fmap pure return fail mapM_ sequence foldMap foldr foldl
            foldr1 foldl1 elem minimum sum product traverse sequenceA mapM id const
            flip until asTypeOf error errorWithoutStackTrace undefined seq filter head
            last tail init null length reverse and or any all concat concatMap scanl
            scanr scanr1 iterate repeat replicate cycle take drop dropWhile takeWhile
            splitAt span break notElem lookup zip zip3 zipWith zipWith3 unzip unzip3
            lines words unlines unwords shows showChar showString showParen readsPrec
            readList reads readParen read lex putChar putStr putStrLn print getChar
            getLine getContents interact readFile writeFile appendFile readIO readLn
            ioError userError))

(setq path-to-ctags "/usr/bin/ctags")

(setq i-keys (mapcar
              (lambda (number) (setq command (format "%c" number)))
              (number-sequence 35 91)))
(nconc i-keys (mapcar
               (lambda (number) (setq command (format "%c" number)))
               (number-sequence 93 250)))

(setq my-unignored-buffers '("*ielm*" "*scratch*" "*ansi-term*" "*term*"))

(setq insert-on-mode #s(hash-table size 30 data (shell-mode insert term-mode term-send-raw-string prog-mode insert!)))

(setq compression-extensions '("7z" "7zip" "zip" "zstd" "tar\..*"))
(defun eval-string (string)
  "Evals all the commands inside the string"
  (eval (car (read-from-string (format "(progn %s)" string)))))

;;; Shell script highlighting
(defun highlight-words-on-mode (mode symbols &optional face)
  "Makes given symbols be highlighted on given mode"
  (setq strings (mapcar 'symbol-name symbols))
  (setq formatted-words
        (concat "\\\\<\\\\(" (mapconcat 'identity
                                        strings "\\\\|") "\\\\)\\\\>"))
  (if face
      (eval-string (format "(font-lock-add-keywords '%s '((\"%s\" .
%s)))" mode formatted-words face))
    (eval-string (format "(font-lock-add-keywords '%s '((\"%s\" .
%s)))" mode formatted-words font-lock-builtin-face))
    )

  ;; highlight options
  (font-lock-add-keywords
   mode
   '(("\\<-[-a-zA-Z0-9]+\\>"
      . font-lock-constant-face)))
  (font-lock-add-keywords
   mode
   `((,(concat "[ \t]+"
               (regexp-opt
                '("!" "!=" "==" "=~") 'paren) "[ \t]+")
      . font-lock-constant-face)))
  )

(defun create-tags (dir-name)
  "Create tags file."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f TAGS -e -R %s" path-to-ctags (directory-file-name dir-name)))
  )
;; from xah-lee http://ergoemacs.org/emacs/elisp_read_file_content.html
(defun re-n-matches ()
  (1- (/ (length (match-data)) 2)))

(defun get-string-from-file (filePath)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))
(defun match-strings-all (&optional string)
  "Return the list of all expressions matched in last search.
      STRING is optionally what was given to `string-match'."
  (loop for i from 0 to (re-n-matches)
        collect (match-string-no-properties i string)))

(defun re-find-all (regexp string &optional groups yes-props)
  "like python's re.find_all"
  (let (
        ;;(groups (or groups (list (regexp-count-capture-groups regexp))))
        (match-string-fun (if (not yes-props) 'match-string 'match-string-no-properties))
        (start 0)
        (matches nil )
        )
    (while (setq start (and (string-match regexp string start) (match-end 0)))
      (setq matches (cons (cdr (match-strings-all string)) matches))
      )
    (setq matches (reverse matches))
    (if (not (cdar matches))
        (mapcar 'car matches)
      matches
      )
    )
  )


(defun apply-eshell-alias (alias &rest definition)
  "basically taken from eshell/alias function"
  (if (not definition)
      (setq eshell-command-aliases-list
            (delq (assoc alias eshell-command-aliases-list)
                  eshell-command-aliases-list))
    (and (stringp definition)
         (set-text-properties 0 (length definition) nil definition))
    (let ((def (assoc alias eshell-command-aliases-list))
          (alias-def (list alias
                           (eshell-flatten-and-stringify definition))))
      (if def
          (setq eshell-command-aliases-list
                (delq def eshell-command-aliases-list)))
      (setq eshell-command-aliases-list
            (cons alias-def eshell-command-aliases-list))))
  )
(defun eshell-load-bashrc-aliases ()
  (interactive)
  (mapc (lambda (alias-def) (apply 'eshell/alias alias-def))
        (re-find-all "^alias \\([^=]+\\)='?\\(.+?\\)'?$"
                     (get-string-from-file  (concat (getenv "HOME") "/" ".alias"))
                     )
        )
  )
(defun make-buffer-uninteresting ()
  "rename the current buffer to begin with a space"
  (interactive)
  (unless (string-match-p "^ " (buffer-name))
    (rename-buffer (concat " " (buffer-name)))))

(defun make-buffer-interesting ()
  "rename the current buffer to begin with a space"
  (interactive)
  (if (string-match-p "^ " (buffer-name))
      (rename-buffer (concat " " (buffer-name)))))
(defun dfeich/ansi-terminal (&optional path name)
  "Opens an ansi terminal at PATH. If no PATH is given, it uses
the value of `default-directory'. PATH may be a tramp remote path.
The ansi-term buffer is named based on `name' "
  (interactive)
  (unless path (setq path default-directory))
  (unless name (setq name "ansi-term"))
  (ansi-term "/bin/bash" name)
  (let ((path (replace-regexp-in-string "^file:" "" path))
    (cd-str
     "fn=%s; if test ! -d $fn; then fn=$(dirname $fn); fi; cd $fn;")
    (bufname (concat "*" name "*" )))
    (if (tramp-tramp-file-p path)
    (let ((tstruct (tramp-dissect-file-name path)))
      (cond
       ((equal (tramp-file-name-method tstruct) "ssh")
        (process-send-string bufname (format
                      (concat  "ssh -t %s '"
                           cd-str
                           "exec bash'; exec bash; clear\n")
                      (tramp-file-name-host tstruct)
                      (tramp-file-name-localname tstruct))))
       (t (error "not implemented for method %s"
             (tramp-file-name-method tstruct)))))
      (process-send-string bufname (format (concat cd-str " exec bash;clear\n")
                       path)))))
(defun ins-mode (str)
  (interactive)
  (setq f (gethash major-mode insert-on-mode))
  (if (eq f nil)
      (insert! str)
    (funcall f str)
    )
  )
(defun ii-mode ()
  (mapcar
   (lambda (key)
     (eval-string
      (format "(map!
:i \"i %s\" (lambda() (interactive) (ins-mode \"i\") (execute-kbd-macro (kbd \"%s\"))))"
              key key))) i-keys)

  ;; Exceptions
  (map! :i "i !" (lambda()
                   (interactive)
                   (ins-mode "i")
                   (execute-kbd-macro (kbd "!"))))
  (map! :i "i \"" (lambda()
                    (interactive)
                    (ins-mode "i")
                    (execute-kbd-macro (kbd  (format "%c" 34)))))
  (map! :i "i SPC" (lambda()
                     (interactive)
                     (ins-mode "i ")
                     ))
  (map! :i "i ESC" (lambda()
                     (interactive)
                     (ins-mode "i")
                     ;; (execute-kbd-macro (kbd "ESC"))))
                     (evil-normal-state)))
  (map! :i "i C-g" (lambda()
                     (interactive)
                     (ins-mode "i")
                     ))
  (map! :i "i RET" (lambda()
                     (interactive)
                     (ins-mode "i")
                     (execute-kbd-macro (kbd "RET"))))
  (map! :i "i \\" (lambda()
                    (interactive)
                    (ins-mode "i")
                    (execute-kbd-macro (kbd "\\"))))
  (map! :i "i DEL" (lambda()
                     (interactive)
                     (ignore)))
  (map! :i "i TAB" (lambda()
                     (interactive)
                     (ins-mode "i")
                     (execute-kbd-macro (kbd "TAB"))))
  (map! :i "i C-n" (lambda()
                     (interactive)
                     (ins-mode "i")
                     (execute-kbd-macro (kbd "C-n"))))
  ;; (evil-complete-next)))

  ;; F keys
  (map! :i "i <f1>" (lambda()
                      (interactive)
                      (ins-mode "i")
                      (execute-kbd-macro (kbd "<f1>"))))
  (map! :i "i <f2>" (lambda()
                      (interactive)
                      (ins-mode "i")
                      (execute-kbd-macro (kbd "<f2>"))))
  (map! :i "i <f3>" (lambda()
                      (interactive)
                      (ins-mode "i")
                      (execute-kbd-macro (kbd "<f3>"))))
  (map! :i "i <f4>" (lambda()
                      (interactive)
                      (ins-mode "i")
                      (execute-kbd-macro (kbd "<f4>"))))
  (map! :i "i <f5>" (lambda()
                      (interactive)
                      (ins-mode "i")
                      (execute-kbd-macro (kbd "<f5>"))))
  (map! :i "i <f6>" (lambda()
                      (interactive)
                      (ins-mode "i")
                      (execute-kbd-macro (kbd "<f6>"))))
  (map! :i "i <f7>" (lambda()
                      (interactive)
                      (ins-mode "i")
                      (execute-kbd-macro (kbd "<f7>"))))
  (map! :i "i <f8>" (lambda()
                      (interactive)
                      (ins-mode "i")
                      (execute-kbd-macro (kbd "<f8>"))))
  (map! :i "i <f9>" (lambda()
                      (interactive)
                      (ins-mode "i")
                      (execute-kbd-macro (kbd "<f9>"))))
  (map! :i "i <f10>" (lambda()
                       (interactive)
                       (ins-mode "i")
                       (execute-kbd-macro (kbd "<f10>"))))
  (map! :i "i <f11>" (lambda()
                       (interactive)
                       (ins-mode "i")
                       (execute-kbd-macro (kbd "<f11>"))))
  (map! :i "i <f12>" (lambda()
                       (interactive)
                       (ins-mode "i")
                       (execute-kbd-macro (kbd "<f12>"))))

  ;; Other special keys
  (map! :i "i <pause>" (lambda()
                         (interactive)
                         (ins-mode "i")
                         (execute-kbd-macro (kbd "<pause>"))))
  (map! :i "i <Scroll_Lock>" (lambda()
                               (interactive)
                               (ins-mode "i")
                               (execute-kbd-macro (kbd "<Scroll_Lock>"))))
  (map! :i "i <insert>" (lambda()
                          (interactive)
                          (ins-mode "i")
                          (execute-kbd-macro (kbd "<insert>"))))
  (map! :i "i <deletechar>" (lambda()
                              (interactive)
                              (ins-mode "i")
                              (execute-kbd-macro (kbd "<deletechar>"))))
  (map! :i "i <home>" (lambda()
                        (interactive)
                        (ins-mode "i")
                        (execute-kbd-macro (kbd "<home>"))))
  (map! :i "i <end>" (lambda()
                       (interactive)
                       (ins-mode "i")
                       (execute-kbd-macro (kbd "<end>"))))
  (map! :i "i <prior>" (lambda()
                         (interactive)
                         (ins-mode "i")
                         (execute-kbd-macro (kbd "<prior>"))))
  (map! :i "i <next>" (lambda()
                        (interactive)
                        (ins-mode "i")
                        (execute-kbd-macro (kbd "<next>"))))

  ;; i i
  (map! :i "i i" #'evil-normal-state)
  )
(defun get-file-type (file &optional deref-symlinks)
  "Return the type of FILE, according to the `file' command.
If you give a prefix to this command, and FILE is a symbolic
link, then the type of the file linked to by FILE is returned
instead."
  (let (process-file-side-effects)
    (with-temp-buffer
      (if deref-symlinks
          (process-file "file" nil t t "-bL" "--" file)
        (process-file "file" nil t t "-b" "--" file))
      (when (bolp)
        (backward-delete-char 1))
      (buffer-string))))
(map! :n "g r" #'evil-replace-with-register)
(evil-replace-with-register-install)
(map! :n "SPC w D" #'delete-other-windows)
(defvar my-term-shell "/bin/zsh")
(setq tramp-shell-prompt-pattern "$ ")
;; (eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))
(defadvice ansi-term (before force-bash)
  (interactive (list my-term-shell)))
(ad-activate 'ansi-term)
(map! :map 'pdf-view-mode-map
      :n "f" #'pdf-links-action-perform)
(map! :n "SPC f z" #'counsel-fzf)
(map! :n "SPC b j" #'ivy-switch-buffer)
(map! :n "SPC j j" #'ivy-switch-buffer)
(map! :map 'ranger-mode-map :g "SPC b j" #'ivy-switch-buffer)
(map! :map 'ranger-mode-map :g "SPC j j" #'ivy-switch-buffer)
(map! :n "SPC o c" #'calendar)
(map! :n "SPC o s" #'ansi-term)

(map! :map 'ranger-mode-map :g "SPC o c" #'calendar)
(map! :map 'ranger-mode-map :g "SPC o s"
      (lambda() (interactive)
        (if (file-remote-p default-directory)
            (dfeich/ansi-terminal)
          (ansi-term my-term-shell))))
(map! :n "SPC j h" (lambda () (interactive) (ansi-term "htop")))
(defun mark-real-user-buffer()
  (interactive)
  (setq buf-name (buffer-name))
  (switch-to-buffer "*scratch*")
  (switch-to-buffer buf-name)
  )
(map! :n "SPC b R" (lambda ()
                     (interactive)
                     (mark-real-user-buffer)
                     ))
(map! :map 'dired-mode-map :n "A" (lambda () (interactive) (find-alternate-file "..")))
(map! :map 'dired-mode-map
      :n "w" #'peep-dired
      )
(map! :map 'dired-mode-map :n "a"
      (lambda ()
        (interactive)
        (if (equal (get-file-type (dired-get-file-for-visit)) "directory")
            (dired-find-alternate-file)
          (dired-find-file))
        ))
(map! :map 'dired-mode-map :n "l"
      (lambda ()
        (interactive)
        (if (equal (get-file-type (dired-get-file-for-visit)) "directory")
            (dired-find-alternate-file)
          (dired-find-file))
        ))
(map! :map 'dired-mode-map :n "SPC m o" #'hydra-dired-quick-sort/body)
(map! :map 'dired-mode-map :n "h" (lambda () (interactive) (find-alternate-file "..")))
(ii-mode)
;; Why is not working?
;; (ii-mode org-mode-map "org-mode-map")
;; (map! :n "SPC i i" (lambda () (ii-mode org-mode-map "org-mode-map")))
(setq dired-dwim-target t)
(map! :map 'peep-dired-mode-map
      :n "j" #'peep-dired-next-file
      :n "k" #'peep-dired-prev-file
      )
(add-hook! 'peep-dired-hook #'evil-normalize-keymaps)
(setq peep-dired-cleanup-on-disable t)
(setq peep-dired-ignored-extensions '("mkv" "iso" "mp4" "webm" "avi"))
(show-paren-mode 1)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook! 'text-mode-hook #'turn-on-auto-fill)
(setq-default auto-fill-function 'do-auto-fill)
;; (require 'whitespace)
;; (setq whitespace-style '(face lines-tail))
;; (add-hook 'text-mode-hook
;;           (setq whitespace-line-column 80)
;;           ;; (global-whitespace-mode t) ; for dot on spaces
;;           )
;; (setq-default show-trailing-whitespace t)
(setq-default indicate-empty-lines t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(global-visual-line-mode)
(global-evil-surround-mode t)
(vimish-fold-global-mode 1)
(add-hook 'prog-mode-hook
          (lambda ()
            (if (or (derived-mode-p 'python-mode) (derived-mode-p 'haskell-mode))
                (progn
                  (yafolding-mode)
                  (yafolding-hide-all)
                  )
              (+fold/close-all)
              )))

(defun yafolding-rec (todo)
  (interactive)
  (setq level (yafolding-get-indent-level))
  (while (or (current-line-empty-p) (> level 0))
    (if (current-line-empty-p)
        (progn
          (re-search-backward "^.")
          (setq level (yafolding-get-indent-level))
          )
      (progn
        (yafolding-go-parent-element)
        (setq level (- level 1))
        )
      ))
  (cond ((eq todo 'hide-element) (yafolding-hide-element))
        ((eq todo 'hide-all) (yafolding-hide-all))
        )
  )

(defun current-line-empty-p ()
  ;; Spaces not included
  (string-match-p "\\`$" (thing-at-point 'line)))
(defun current-line-empty2-p ()
  ;; Spaces included
  (string-match-p "\\`\\s-*$" (thing-at-point 'line)))

(map! :map 'hs-minor-mode-map
      :n "z o" #'hs-show-block
      )

(map! :map 'yafolding-mode-map
      :n "z a" #'yafolding-toggle-fold
      :n "z c" #'yafolding-hide-parent-element
      :n "z C" (lambda () (interactive) (yafolding-rec 'hide-element))
      :n "z o" #'yafolding-show-element
      :n "z O" #'yafolding-show-element
      :n "z m" (lambda () (interactive) (yafolding-rec 'hide-all))
      :n "z r" #'yafolding-show-all
      )
;; Default user.
(setq rcirc-default-nick "initega")
(setq rcirc-default-user-name "initega")
(setq rcirc-default-full-name "initega")
(add-hook 'eshell-mode-hook (lambda ()
                              (if (eq company-mode nil)
                                  (company-mode)
                                )
                              ))
(setq shell-command-switch "-ic")

(defun singpolyma/term-insert-literal (key)
  "Take a keypress and insert it literally into a terminal."
  (interactive "cPress key:")
  (term-send-raw-string (format "\e%c" key))
  )

(add-hook 'term-mode-hook
          (lambda ()
            (interactive)
            (doom-mark-buffer-as-real-h)
            (map!
             :i "C-#" #'singpolyma/term-insert-literal
             )))
(defalias 'open 'find-file)
(defalias 'openo 'find-file-other-window)
;; (apply 'eshell/alias '("config" "/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME"))
;; (add-hook 'eshell-mode-hook 'eshell-load-bashrc-aliases)
(defun my-ido-ignore-func (name)
  "Ignore all non-user (a.k.a. *starred*) buffers except those listed in `my-unignored-buffers'."
  (and (string-match "^\*" name)
       (not (member name my-unignored-buffers))))

(setq ido-ignore-buffers '("\\` " my-ido-ignore-func))
(add-hook 'calendar-load-hook
          (lambda ()
            (calendar-set-date-style 'european)))
(setq calendar-week-start-day 1)
(setq flycheck-flake8-maximum-line-length 80)
(add-to-list 'load-path "~/.doom.d/lisp/")
(load "rate-sx")
(load "sunrise")
(after! haskell-mode
  (progn
    (setq
     ;; Format files with Brittany instead of Stylish
     haskell-mode-stylish-haskell-path "brittany"
     ;; Format buffer with Brittany on save
     haskell-stylish-on-save t
     ;; Suggest removing imports
     haskell-process-suggest-remove-import-lines t)))
(add-hook 'haskell-mode-hook
          (lambda ()
            (interactive)
            (haskell-indentation-mode)
            (interactive-haskell-mode)
            (yas-minor-mode)
            ))

(map! :map 'haskell-interactive-mode-map
      :i "C-j" #'haskell-interactive-mode-history-next)
(map! :map 'haskell-interactive-mode-map
      :i "C-k" #'haskell-interactive-mode-history-previous)
;; (setq haskell-process-path-ghci "stack-ghci")
(setq haskell-process-type 'stack-ghci)
(after! dante
  (nconc dante-methods '(stack-ghci))
  (defun dante-repl-command-line ()
    '("stack-ghci"))
  )
;; (setq dante-methods
;; '(styx new-impure-nix new-nix nix impure-nix new-build nix-ghci stack mafia
;; bare-cabal bare-ghci)))

(add-hook 'dante-mode-hook
          '(lambda () (flycheck-add-next-checker 'haskell-dante
                                                 '(warning . haskell-hlint))))
(highlight-words-on-mode 'haskell-mode prelude-symbols)
(setq org-babel-load-languages '((emacs-lisp . t) (ein . t)))
(elpy-enable)

;; Following disabled due to high CPU usage
;;; Use IPython for REPL
;; (setq python-shell-interpreter "jupyter"
;;       python-shell-interpreter-args "console --simple-prompt"
;;       python-shell-prompt-detect-failure-warning nil)
;; (add-to-list 'python-shell-completion-native-disabled-interpreters
;;              "jupyter")
;; (setq python-shell-interpreter "ipython"
;;       python-shell-interpreter-args "-i --simple-prompt")

(setq blacken-line-length 80)

(add-hook 'python-mode-hook
          (lambda ()
            (setq-default indent-tabs-mode nil)
            (setq-default tab-width 4)
            (yafolding-mode)
            (setq-default python-indent 4)))
(highlight-words-on-mode 'sh-mode sh-symbols)
;; (add-to-list 'TeX-mode-hook #'TeX-fold-buffer)
;; (add-hook! 'tex-mode-hook #'TeX-fold-buffer)
(defun my-shell ()
    (interactive)
    (let ((default-directory "/ssh:initega@192.168.0.28:"))
      (eshell)))
(setq user-full-name "inigoortega"
      user-mail-address "inigoortega@tutanota.com")
