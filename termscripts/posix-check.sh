#!/usr/bin/env sh

output="/tmp/posix-check-$(date +%s)"
checkbashisms -f $SCRIPTS/*.sh $TSCRIPTS/*.sh > $output 2>&1 

errorline () {
    [ -n "$(printf "%b\n" "$1" | grep '(unsafe printf "%b\n" with backslash)')" ] && printf "%b\n" "1" && \
        return
    [ -n "$(printf "%b\n" "$1" | grep '($"foo" should be eval_gettext "foo")')" ] && \
        printf "%b\n" "1" && return
    [ -n "$(printf "%b\n" "$1" | grep '(\[^\] should be \[!\])')" ] && printf "%b\n" "1" && return
    [ -n "$(printf "%b\n" "$1" | grep -E "bashism in ($SCRIPTS|$TSCRIPTS)/.*\\.sh")" ] && printf "%b\n" "2" && return
    printf "%b\n" "0"
}

bashism="0"
while read -r line
do
    # printf "%b\n" "$(errorline "$line")"
    case "$(errorline "$line")" in
        0) ;;
        1) bashism="0" ;;
        2) bashism="1" ;;
    esac
    [ "$bashism" = "1" ] && printf "%b\n" "$line"
done < $output
