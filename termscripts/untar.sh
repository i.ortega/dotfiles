#!/usr/bin/env sh

for i in `seq 1 $#`
do
    tar -xvf $1
    shift
done
