#!/bin/sh

[ -z $1 ] && exit 1

if [ "$#" -gt 1 ]; then
    for i in "$@"
    do
        # echo "$i"
        dash "$0" "$i"
    done
else
    if [ -d "$1" ]; then
        dash "$0" "$1"/*
    elif [ -f "$1" ]; then
        echo "$1"
        first="$(sed -E 's|^#!/bin/sh$|#!/usr/bin/env sh|g' "$1")"
        second="$(echo "$first" | sed 's|mksh|dash|g')"
        third="$(echo "$second" | sed 's|echo|printf "%b\\n"|g')"
        echo "-----------------------------------------"
        new="/tmp/posixfy$(date +%s)"
        echo "$third" > "$new"
        difference="$(diff -u "$1" "$new")"
        [ -z "$difference" ] && exit 0 || echo "$difference"
        dash $TSCRIPTS/yes-or-no.sh "Confirm?" && echo "$third" > "$1"
        echo "-----------------------------------------"
    else
        echo "Unexpected error" 1>&2
        exit 1
    fi
fi
