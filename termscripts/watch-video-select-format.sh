#!/usr/bin/env sh

select_format() {
    youtube-dl -F "$1" | awk '/format code/{p=1;next}{if(p){print}}' |
        cut -d ' ' -f 1 | tac | dmenu -i
}

watch="$1"

if [ "$(printf "No\nYes" | dmenu -i -p "Select Format?")" = "Yes" ]; then
    format=$(select_format "$watch")
    [ -n "$format" ] && $VIDEOPLAYER_SELECT_YTDLFORMAT$format "$watch"
else
    $VIDEOPLAYER "$watch"
fi
