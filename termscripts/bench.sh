#!/usr/bin/env sh

case $1 in
    dash) strace -cf dash -c 'for i in $(seq 1 1000); do dash -c "$(cat \
        ~/termscripts/cosas.sh)"; done' ;;
    mksh) strace -cf dash -c 'set -o posix; for i in $(seq 1 1000); do dash -c "$(cat \
        ~/termscripts/cosas.sh)"; done' ;;
    ksh) strace -cf ksh -c 'for i in $(seq 1 1000); do ksh -c "$(cat \
        ~/termscripts/cosas.sh)"; done' ;;
    bash) strace -cf bash -c 'for i in $(seq 1 1000); do bash -c "$(cat \
        ~/termscripts/cosas.sh)"; done' ;;
    ash) strace -cf busybox ash -c 'for i in $(seq 1 1000); do busybox ash -c \
        "$(cat ~/termscripts/cosas.sh)"; done' ;;
    sh) strace -cf busybox sh -c 'for i in $(seq 1 1000); do busybox sh -c \
        "$(cat ~/termscripts/cosas.sh)"; done' ;;
    zsh) strace -cf zsh -c 'for i in $(seq 1 1000); do zsh -c \
        "$(cat ~/termscripts/cosas.sh)"; done' ;;
    fish) strace -cf fish -c 'for i in (seq 1 1000); do fish -c "(cat \
        ~/termscripts/cosas.sh)"; done' ;;
esac
