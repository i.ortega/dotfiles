#!/usr/bin/env sh

[ -z "$1" ] && exit 1

if [ "$1" = "-dec" ]; then
    operator="-"
elif [ "$1" = "-inc" ]; then
    operator="+"
fi

[ -z "$2" ] && exit 1

[ "$1" != "-set" ] && result="$(printf "%b\n" "$BRIGHTNESS $operator $2" | bc -l)" \
    || result="$2"

[ "$(printf "%b\n" "$result > 1.0" | bc -l)" -eq 1 ] && result="1.0"
[ "$(printf "%b\n" "$result < 0.0" | bc -l)" -eq 1 ] && result="0.0"

xrandr --output LVDS1 --brightness "$result" && export BRIGHTNESS="$result"
