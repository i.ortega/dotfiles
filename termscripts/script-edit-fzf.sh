#!/usr/bin/env sh

file="$(find -L "$SCRIPTS" "$TSCRIPTS" -regex ".*\.\(ba\)?sh\$" \
    -type f | sed "s|^$HOME/||" | fzf -i)"
[ -z "$file" ] || $EDITOR "$HOME/$file"
