#!/usr/bin/env sh

while true; do
    read -p "$* [Y/n]: " yn
    case $yn in
        "") return 0  ;;
        [Yy]*) return 0  ;;  
        [Nn]*) printf "%b\n" "Aborted" ; return  1 ;;
    esac
done
